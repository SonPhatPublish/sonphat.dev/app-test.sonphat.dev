/* SonPhat Product Publish Config */
SP[`Products`] = [
    // {"STT": 1, "NameApp": "SbusDriver (2022-01-06 17h15p00s)", "Link": "/apks/SbusDriver.2022-01-06-17-15-00.apk"},
    // {"STT": 2, "NameApp": "SbusDriver (2021-12-30 19h05p00s)", "Link": "/apks/SbusDriver.2021-12-30-19-05-00.apk"},

    // QC-Test
    // {"STT": 2, "NameApp": "SbusDriver ( QC-01 / 2022-01-18 16h17p00s )", "Link": "/apks/SbusDriver.2022-01-18-16-17-00.apk"},
    // {"STT": 2, "NameApp": "SbusDriver ( QC-01 / fix print )", "Link": "/apks/SbusDriver.2022-01-20-17-56-00.apk"},
    // {"STT": 2, "NameApp": "SbusDriver ( QC-01 / 2022-01-27 09h35m00s )", "Link": "/apks/SbusDriver.2022-01-27-09-35-00.apk"},

    // {"STT": 2, "NameApp": "SbusDriver ( BETA / 2022-02-17 14h47m00s )", "Link": "/apks/SbusDriver.2022-02-17-14-47-00.apk"},
    // {"STT": 2, "NameApp": "SbusDriver ( BETA / 2022-02-17 15h03m00s )", "Link": "/apks/SbusDriver.2022-02-17-15-03-00.apk"},
    // {"STT": 1, "NameApp": "SbusDriver ( BETA / 2022-02-27 22h05m00s )", "Link": "/apks/SbusDriver.2022-02-27-22-05-00.apk"},
    // {"STT": 1, "NameApp": "SbusDriver ( BETA / 2022-02-27 23h20m00s )", "Link": "/apks/SbusDriver.2022-02-27-23-20-00.apk"},
    // {"STT": 1, "NameApp": "SbusDriver ( REVIEW / 2022-03-20 23h29m00s )", "Link": "/apks/SbusDriver.2022-03-20-23-29-00.apk"},
    // {"STT": 1, "NameApp": "SbusDriver ( REVIEW / 2022-03-21 17h47m00s )", "Link": "/apks/SbusDriver.2022-03-21-17-47-00.apk"},
    // {"STT": 1, "NameApp": "SbusDriver ( 1.5.11.1-RC / 2022-05-02 23h20m00s )", "Link": "/apks/SbusDriver.2022-05-02-23-20-00.apk"},
    // {"STT": 1, "NameApp": "SbusDriver ( 1.5.12.1-RC / 2022-05-04 17h52m00s )", "Link": "/apks/SbusDriver.2022-05-04-17-52-00.apk"},
    // {"STT": 1, "NameApp": "SLaiXe ( 1.5.14.1-RC / 2022-06-28 00h05m00s )", "Link": "/apks/SbusDriver.2022-06-28-00-07-00.apk"},
    // {"STT": 1, "NameApp": "SLaiXe ( 1.5.17.1-RC / 2022-09-20 13h15m00s )", "Link": "/apks/SbusDriver.2022-09-20-13-15-00.apk"},
    {"STT": 1, "NameApp": "SLaiXe ( 1.5.18.1-RC / 2022-10-01 01h22m00s )", "Link": "/apks/SbusDriver.2022-10-01-01-22-00.apk"},
    
    /* Utils */
    {"STT": 2, "NameApp": "RawBT 4.8.3", "Link": "/apks/rawbt.4.8.3.patched.apk"},

    /* LenhDienTu.vn */
    {"STT": 3, "NameApp": "Lệnh Điện Tử ( 1.0.0+1 / 2022-10-07 14h44m00s )", "Link": "/apks/vn.lenhdientu/LenhDienTu.2022-10-07-14-44-00.apk"},
];
/* SonPhat Product Publish Config */
