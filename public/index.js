var container = document.querySelector("#container");
var rowTem = document.querySelector("#rowTem");


setTimeout(_ => {
    let Products = SP["Products"];

    for (var pro of Products || []) {
        var row = rowTem.content.cloneNode(true);
        var propEls = Array.from(row.querySelectorAll("[prop-name]"));

        for (var propEl of propEls) {
            var propName = propEl.getAttribute("prop-name") || false;
            var propFor = propEl.getAttribute("prop-for") || "textContent";

            if (!!propName && pro.hasOwnProperty(propName)) {
                propEl[propFor] = pro[propName];
            }
        }

        container.appendChild(row);

        // break;
    }
}, 200);
